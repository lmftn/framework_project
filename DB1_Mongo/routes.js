var express = require('express');
var router = express();
var productsRts = require('./Routes/productsRts');
var categoriesRts = require('./Routes/categoriesRts');

//ESSENTIAL !!!!!!!!!!
//Need the body parser lines to be able to get the req.body !!
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({extended: false}));
router.use(bodyParser.json());

router = productsRts(router);
router = categoriesRts(router);

var port = 1800;
router.listen(port, function(){ console.log('DB1 started, listening on port ' + port) });