var mongoose = require('../DB1_Connection');
var Schema = mongoose.Schema;

var transactionSchema = new Schema({
  buyed_id: { type: String, required: true },
  product_id: { type: String, required: true },
  price: { type: Number, required: true },
  transaction_date: { type: Date, default: Date.now }
});

var Transaction = mongoose.model('Transaction', transactionSchema);
module.exports = Transaction;