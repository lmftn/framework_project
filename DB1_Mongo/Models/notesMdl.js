var mongoose = require('../DB1_Connection');
var Schema = mongoose.Schema;

var noteSchema = new Schema({
  note: { type: Number, required: true },
  product_id: { type: String, required: true },
  user_id: { type: String, required: true },
});

var Note = mongoose.model('Note', noteSchema);
module.exports = Note;