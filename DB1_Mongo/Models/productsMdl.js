var mongoose = require('../DB1_Connection');
var Schema = mongoose.Schema;

var productSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  price: { type: Number, required: true },
  //category: { type: String, required: true },
  seller_id: { type: Number, required: true },
  creation_date: { type: Date, default: Date.now }
});

var Product = mongoose.model('Product', productSchema);
module.exports = Product;