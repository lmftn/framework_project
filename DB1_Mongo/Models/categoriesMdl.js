var mongoose = require('../DB1_Connection');
var Schema = mongoose.Schema;

var categorySchema = new Schema({
  name: { type: String, required: true },
  parent_id: { type: String, default: 0 },
});

var Category = mongoose.model('Category', categorySchema);
module.exports = Category;