var db = require('../DB1_Connection');
var Product = require('../Models/productsMdl');
var express = require('express');
var router = express();

//All exported functions that goes on DB
exports.productCreate = function(req, res, next){
	var newProduct = new Product(req.body);
	newProduct.save(function(err){
		if(err) {
			if (err.code == 11000) res.status(404).json({ error: '404 error: Duplicate key' });
			else res.status(404).json({ error: '404 error: Not found' });
		}
		else res.status(200).json('Product created');		
	});
}

exports.productReadOne = function(req, res, next){
	Product.findOne({"_id": req.params.pid}, function(err, data){
		res.json(data);
	});
}

exports.productReadAll = function(req, res, next){
	var query = Product.find();
	query.select().exec(function(err, data){
		if(err) throw err;
		res.json(data);
	})
}

exports.productUpdate = function(req, res, next){
	//pid is the id of the object
	//first, we find the product
	Product.findOne({"_id": req.params.pid}, function(err, product){
		if(err) res.json('ERROR PRODUCT UPDATE');
		else {
			
	// 		//then we make an array with all the keys of the product
	var arrProd = Object.keys(product._doc);
	// 		//for each keys we check if a req.body was given
	arrProd.forEach(function(k){
		if(req.body[k]){
			product[k] = req.body[k];
		}
	});
	product.save(function(err){
		if(err) throw err;
		else res.status(200).json(product);
	});	
}
});
}

exports.productDelete = function(req, res, next){
	Product.findByIdAndRemove(req.params.pid, (err, product) => {
		if(err){
			res.status(400).json("ERROR DELETE PRODUCT");
		}
		else {
			res.status(200).json('Successfully deleted');
		}
	});
}

exports.productDeleteByUser = function(req, res, next){
	Product.remove({"user_id": req.params.pid}, function(err){
		if(err) res.json("Cannot delete product by user ID");
	})
}
