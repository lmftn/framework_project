var db = require('../DB1_Connection');
var Category = require('../Models/categoriesMdl');
var Product = require('../Models/productsMdl');
var express = require('express');
var router = express();

exports.categoryCreate = function(req, res, next){
	var newCategory = new Category(req.body);
	newCategory.save(function(err){
		if (err) throw err;
		else res.status(200).json('Category created');
	});
}

exports.categoryReadAll = function(req, res, next){
	var query = Category.find();
	query.select().exec(function(err, data){
		if (err) throw err;
		res.json(data);
	});
}

exports.categoryProducts = function(req, res, next){
	Category.findOne({"name": req.params.cname}, function(err, data){
		if(err) throw err;
		Product.findOne({"category_id": data._id}, function(err, data){
			if(err) throw err;
			res.json(data);
		});
	});
}

exports.categoryUpdate = function(req, res, next){
	Category.findOne({'_id': req.params.cid}, function(err, category){
		if(err) throw err;
		else {
			var arrCat = Object.keys(category._doc);
			arrCat.forEach(function(k){
				if(req.body[k]) category[k] = req.body[k];
			});
			category.save(function(err){
				if(err) throw err;
				else res.status(200).json('Category modified');
			});
		}
	});
}

//need to change all products of the deleted category to category non-classé
exports.categoryDelete = function(req, res, next){
	Category.findOne({'parent_id': req.params.cid}, function(err, data){
		if(err) throw err;
		if(data) res.json('Enabled to delete this category, please delete sub-categories first');
		else {
			Category.remove({'_id': req.params.cid});
			res.json('Category deleted');
		}
	});
}












