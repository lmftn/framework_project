module.exports = function(router) {

	var productsCtrl = require('../Controllers/productsCtrl');

	//get all products
	router.get('/products', function(req, res, next) {
	  productsCtrl.productReadAll(req, res, next);
	});

	//get one product
	router.get('/product/:pid', function(req, res, next) {
	  productsCtrl.productReadOne(req, res, next);
	})

	//create a product
	router.post('/product', function(req, res, next){
		productsCtrl.productCreate(req, res, next);
	});

	router.put('/product/:pid', function(req, res, next){
		productsCtrl.productUpdate(req, res, next);
	});

	router.delete('/product/:pid', function(req, res, next){
		productsCtrl.productDelete(req, res, next);
	});

	router.delete('/product/user/:uid', function(req, res, next){
		productsCtrl.productDeleteByUser(req, res, next);
	})

	return (router);
}
