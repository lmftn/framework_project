module.exports = function(router) {

	var categoriesCtrl = require('../Controllers/categoriesCtrl');
	// create a new category
	router.post('/category', function(req, res, next) {
	  categoriesCtrl.categoryCreate(req, res, next);
	});

	//get all categories to choose one
	router.get('/categories', function(req, res, next){
		categoriesCtrl.categoryReadAll(req, res, next);
	});

	//get all products from a category
	router.get('/category/:cname', function(req, res, next){
		categoriesCtrl.categoryProducts(req, res, next);
	});

	//to update a category
	router.put('/category/:cid', function(req, res, next){
		categoriesCtrl.categoryUpdate(req, res, next);
	});

	//to delete a category and its children ...
	router.delete('/category/:cid', function(req, res, next){
		categoriesCtrl.categoryDelete(req, res, next);
	});

	return (router);
}
