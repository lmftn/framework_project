import { Component, OnInit } from '@angular/core';
import { UserManagementService } from '../user-management.service';
import { AuthService } from '../auth.service';
import { JwtHelper } from 'angular2-jwt';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
	uid: string;
  user = [];
  admin: boolean;
  status;

  constructor(private userManagementService: UserManagementService, private authService: AuthService) { }

  ngOnInit() {
  	let jwtHelper: JwtHelper = new JwtHelper();
  	this.uid = jwtHelper.decodeToken(localStorage.getItem('token'))['identity'];
  	this.showProfile();
    this.authService.isAdmin().subscribe(data => {this.status = data[0]['status'], this.boolAdmin(this.status)});
  }

  boolAdmin(stat){
    if(stat == "admin"){
      this.admin = true;
    }
  }
  
  showProfile(){
    this.userManagementService.userProfile(this.uid).subscribe(
           data => this.user = data,
           error => console.log(error),
        )
  }

}
