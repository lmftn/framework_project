import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs/Observable";

@Injectable()
export class UserManagementService {

  constructor(private http: Http) {}
   
  	usersList() {
  		return this.http.get('http://127.0.0.1:5000/users')
  		.map((res: Response) => res.json());
  	}

  	userProfile(uid){
  		return this.http.get('http://127.0.0.1:5000/user/'+uid)
  		.map((res: Response) => res.json());
  	}

  	register(body){
  		return this.http.post('http://127.0.0.1:5000/register', body)
  		.map((res: Response) => res.json());
  	}

    login(body){
      return this.http.post('http://127.0.0.1:5000/auth', body)
      .map((res: Response) => res.json());
    }

    userUpdate(uid, body){
      return this.http.put('http://127.0.0.1:5000/user/'+uid, body)
      .map((res: Response) => res.json());
    }

    userDelete(uid){
      return this.http.delete('http://127.0.0.1:5000/user/'+uid)
      .map((res: Response) => res.json());
    }

}
