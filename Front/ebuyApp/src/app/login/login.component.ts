import { Component, OnInit } from '@angular/core';
import { UserManagementService } from '../user-management.service';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	username: string;
	password: string;
	user = [];
  error = [];

  constructor(private appComponent: AppComponent, private userManagementService: UserManagementService, private authService: AuthService, private router: Router) { }
  submitted: boolean = false;

  ngOnInit() {
  }

 onSubmit(event){
  	this.submitted = true;
    this.error = [];
  	let body = {
  		username: this.username,
  		password:this.password,
  	}
  	this.userManagementService.login(body).subscribe(data => {
      localStorage.setItem("token", data.access_token),
  		this.user = data,
      this.appComponent.reload("products")},
      error => this.error = ['Invalid Credentials']);
  }

}
