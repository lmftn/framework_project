import { tokenNotExpired, JwtHelper } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class AuthService {
	status: string;

	constructor(private http: Http) {
	}

	loggedIn(){
		return tokenNotExpired('token');
	}

	isAdmin(){
		let jwtHelper: JwtHelper = new JwtHelper();
  		return this.http.get('http://127.0.0.1:5000/user/'+jwtHelper.decodeToken(localStorage.getItem('token'))['identity'])
  		.map((res: Response) => res.json());
	}

	logID(){
		let jwtHelper: JwtHelper = new JwtHelper();
		return jwtHelper.decodeToken(localStorage.getItem('token'))['identity'];
	}

	loggout(){
		localStorage.clear();
	}
}