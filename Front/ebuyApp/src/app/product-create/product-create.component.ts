import { Component, OnInit } from '@angular/core';
import { ProductManagementService } from '../product-management.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
	name: string;
	description: string;
	price: string;
  //fix_price: string;
	product = [];

  constructor(private productManagementService: ProductManagementService, private authService: AuthService, private router: Router) {}

  ngOnInit() {
  }

  onSubmit(event){
  	let body = {
  		name: this.name,
  		description: this.description,
  		price: this.price,
      //fix_price: this.fix_price,
  		seller_id: this.authService.logID(),
  	}
  	this.productManagementService.productCreate(body).subscribe(data => {
  		this.product=data
  		console.log(this.product)
  	});
    this.router.navigate(['./products']);
  }

}
