import { Component, OnInit } from '@angular/core';
import { UserManagementService } from '../user-management.service';
import { ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { ProductManagementService } from '../product-management.service';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.css']
})
export class UserUpdateComponent implements OnInit {

  constructor(private userManagementService: UserManagementService, private route: ActivatedRoute, private authService: AuthService, private router: Router, private productManagementService: ProductManagementService) { }
	user = [];
	uid: number;
	submitted: boolean = false;
	email: string;
	password: string;
	pwdconfirmation: string;
	first_name: string;
	last_name: string;
	address: string;
	town: string;
	postcode: string;
  status: string;
	newuser = [];
  id;

  ngOnInit() {
  	this.route.params.subscribe(params => {
			this.uid = +params['uid'];
		});
		this.userUpdate();
  }

attri(u){
	this.email = u[0]['email'];
  this.first_name = u[0]['first_name'];
  this.last_name = u[0]['last_name'];
  this.address = u[0]['address'];
  this.postcode = u[0]['postcode'];
  this.town = u[0]['town'];
  this.status = u[0]['status'];
  this.id = u[0]['id'];
  }
  
  userUpdate(){
  	this.userManagementService.userProfile(this.uid).subscribe(
           data => {this.user = data, this.attri(data)},
           error => console.log(error),
        )
  }

  onSubmit(event){
  	this.submitted = true;
  	let body = {
  		email: this.email,
  		password:this.password,
  		pwdconfirmation: this.pwdconfirmation,
  		first_name: this.first_name,
  		last_name: this.last_name,
  		address: this.address,
  		town:this.town,
  		postcode:this.postcode,
      status:this.status,
  	}
  	this.userManagementService.userUpdate(this.uid, body).subscribe(data => {
  		this.user=[data]
  	});
    this.router.navigate(['./profile/'+this.uid]);
 }

 onDelete(event){
    this.userManagementService.userDelete(this.uid).subscribe(
                                                              data => this.user = [data],
                                                              error => console.log(error)
                                                              )
    this.productManagementService.productsDeleteByUser(this.uid).subscribe(
                                                              data => this.user = [data],
                                                              error => console.log(error)

                                                                          )
    this.router.navigate(['./users']);
  }


}
