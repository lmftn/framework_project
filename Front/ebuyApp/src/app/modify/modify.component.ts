import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { UserManagementService } from '../user-management.service';
import { JwtHelper } from 'angular2-jwt';

@Component({
	selector: 'app-modify',
	templateUrl: './modify.component.html',
	styleUrls: ['./modify.component.css']
})
export class ModifyComponent implements OnInit {

	constructor(private userManagementService: UserManagementService, private authService: AuthService) { }
	user = [];
	uid: number;
	submitted: boolean = false;
	email: string;
	password: string;
	pwdconfirmation: string;
	first_name: string;
	last_name: string;
	address: string;
	town: string;
	postcode: string;
	status: string;
	newuser = [];

	ngOnInit() {
		let jwtHelper: JwtHelper = new JwtHelper();
  		this.uid = jwtHelper.decodeToken(localStorage.getItem('token'))['identity'];
		this.userUpdate();
	}

	attri(u){
		this.email = u[0]['email'];
		this.first_name = u[0]['first_name'];
		this.last_name = u[0]['last_name'];
		this.address = u[0]['address'];
		this.postcode = u[0]['postcode'];
		this.town = u[0]['town'];
		this.status = u[0]['status'];
	}

	userUpdate(){
		this.userManagementService.userProfile(this.uid).subscribe(
		                                                           data => {this.user = data, this.attri(data)},
		                                                           error => console.log(error),
		                                                           )
	}

	onModif(event){
		this.submitted = true;
		let body = {
			email: this.email,
			password:this.password,
			pwdconfirmation: this.pwdconfirmation,
			first_name: this.first_name,
			last_name: this.last_name,
			address: this.address,
			town:this.town,
			postcode:this.postcode,
			status:this.status,
		}
		this.userManagementService.userUpdate(this.uid, body).subscribe(data => {
			this.user=data
		});
	}

	onDelete(event){
		this.userManagementService.userDelete(this.uid).subscribe(
		                                                          data => this.user = data,
		                                                          error => console.log(error)
		                                                          )
	}

}
