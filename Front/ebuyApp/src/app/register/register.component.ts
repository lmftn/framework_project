import { Component, OnInit } from '@angular/core';
import { UserManagementService } from '../user-management.service';
import { NgForm } from '@angular/forms';
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	email: string;
	password: string;
	pwdconfirmation: string;
	first_name: string;
	last_name: string;
	address: string;
	town: string;
	postcode: string;
	birthdate: string;
	user = [];

  constructor(private userManagementService: UserManagementService) { }
   submitted: boolean = false;

  ngOnInit() {
  }

   onSubmit(event){
  	this.submitted = true;
  	let body = {
  		email: this.email,
  		password:this.password,
  		pwdconfirmation: this.pwdconfirmation,
  		first_name: this.first_name,
  		last_name: this.last_name,
  		address: this.address,
  		town:this.town,
  		postcode:this.postcode,
  		birthdate: this.birthdate,
  	}
  	this.userManagementService.register(body).subscribe(data => {
  		this.user=data
  		console.log(this.user)
  	});
 }
}