import { Component, OnInit } from '@angular/core';
import { ProductManagementService } from '../product-management.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
	products = [];

  constructor(private productManagementService: ProductManagementService, private router: Router) { }

  ngOnInit() {
  	this.allProducts();
  }

  allProducts(){
  	this.productManagementService.allProducts().subscribe(
        data => this.products = data,
        error => console.log(error)
     )
  }

  productDetails(pid){
    this.router.navigate(['./product/'+pid]);
  }
}
