import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs/Observable";

@Injectable()
export class ProductManagementService {

  constructor(private http: Http) {}

  allProducts() {
      return this.http.get('http://127.0.0.1:5000/product')
      .map((res: Response) => res.json());
    }

   productDetails(pid){
   	return this.http.get('http://127.0.0.1:5000/product/'+pid)
   	.map((res: Response) => res.json());
   }

   productsDeleteByUser(uid){
   	return this.http.delete('http://127.0.0.1:5000/product/user'+uid)
   	.map((res: Response) => res.json());
   }

   productUpdate(pid, body){
       return this.http.put('http://127.0.0.1:5000/product/'+pid, body)
      .map((res: Response) => res.json());
   }

   productDelete(pid){
     return this.http.delete('http://127.0.0.1:5000/product/'+pid)
     .map((res: Response) => res.json());
   }

   productCreate(body){
     return this.http.post('http://127.0.0.1:5000/product', body)
     .map((res: Response) => res.json());
   }
}