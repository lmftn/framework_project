import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductManagementService } from '../product-management.service';
import { UserManagementService} from '../user-management.service';
import { AuthService } from '../auth.service';
import { AdminGuard } from '../admin-guard.service';


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
	product = [];
	pid: string;
  canModify: boolean;
  status;
  admin: boolean = false;

  constructor(private userManagementService: UserManagementService, private productManagementService: ProductManagementService, private route: ActivatedRoute, private authService: AuthService, private adminGuard: AdminGuard) { }

  ngOnInit() {
  	this.route.params.subscribe(params => {
			this.pid = params['pid'];
		});
		this.productDetails();
  this.authService.isAdmin().subscribe(data => {this.status = data[0]['status'], this.boolAdmin(this.status)});
  }

  boolAdmin(stat){
    if(stat == "admin"){
      this.admin = true;
    }
  }
  sellerInfo(uid){
   if(uid == this.authService.logID()){
      this.canModify = true;
    }
    this.userManagementService.userProfile(uid).subscribe(
           data => {this.product[0]['user'] = data[0]['first_name'] + ' ' + data[0]['last_name'] },
           error => console.log(error),
        )
  }

  productDetails(){
  	this.productManagementService.productDetails(this.pid).subscribe(
           data => {this.product = [data], this.sellerInfo(data['seller_id'])},
           error => console.log(error),
        )
  }

  

}
