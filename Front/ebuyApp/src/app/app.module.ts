import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { UserManagementService } from './user-management.service';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './auth.service';
import { ProductsComponent } from './products/products.component';
import { AuthGuard } from './auth-guard.service';
import { UserUpdateComponent } from './user-update/user-update.component';
import { AdminGuard } from './admin-guard.service';
import { ErrorComponent } from './error/error.component';
import { ProfileComponent } from './profile/profile.component';
import { UsersComponent } from './users/users.component';
import { ModifyComponent } from './modify/modify.component';
import { ProductManagementService } from './product-management.service';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductUpdateComponent } from './product-update/product-update.component';
import { ProductCreateComponent } from './product-create/product-create.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    UserDetailComponent,
    LoginComponent,
    ProductsComponent,
    UserUpdateComponent,
    ErrorComponent,
    UsersComponent,
    ProfileComponent,
    ModifyComponent,
    ProductDetailComponent,
    ProductUpdateComponent,
    ProductCreateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule
  ],
  providers: [UserManagementService, ProductManagementService, AuthService, AuthGuard, AdminGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
