import { Component, OnInit } from '@angular/core';
import { ProductManagementService } from '../product-management.service';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {

  constructor(private productManagementService: ProductManagementService, private route: ActivatedRoute, private router: Router, private authService: AuthService) { }
  product = [];
  pid: string;
  name: string;
  description: string;
  price: string;

  ngOnInit() {
  	this.route.params.subscribe(params => {
			this.pid = params['pid'];
		});
	this.productUpdate();
  }

  attri(p){
  	this.name = p['name'];
  	this.description = p['description'];
  	this.price = p['price'];
  }

  productUpdate(){
  	this.productManagementService.productDetails(this.pid).subscribe(
           data => {this.product = [data], this.attri(data)},
           error => console.log(error),
        )
  }

  delay(ms: number){
  	return new Promise<void>(function(resolve){
  		setTimeout(resolve, ms);
  	});
  }

  async onSubmit(event){
  	let body = {
  		name: this.name,
  		description: this.description,
  		price: this.price,
  	}
  	this.productManagementService.productUpdate(this.pid, body).subscribe(data => {
  		this.product = [data]
  	});
  	await this.delay(500);
  	this.router.navigate(['./product/'+this.pid]);
  }

 async onDelete(event){
    this.productManagementService.productDelete(this.pid).subscribe(
                                                              data => this.product = [data],
                                                              error => console.log(error)
                                                              );
    await this.delay(500);
    this.router.navigate(['./products']);
  }
}
