import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
	constructor(private authService: AuthService, private router: Router){}
	title = 'Ebuy';
	show: boolean;
	admin: boolean = false;
	status;

	ngOnInit() {
		this.show = this.authService.loggedIn();
		this.authService.isAdmin().subscribe(data => {this.status = data[0]['status'], this.boolAdmin(this.status)});
	}

	boolAdmin(stat){
		if(stat == "admin"){
			this.admin = true;
		}
	}

	delay(ms: number){
    return new Promise<void>(function(resolve){
      setTimeout(resolve, ms);
    });
  }

	async reload(place){
		this.router.navigate(['./'+place]);
		await this.delay(500);
		location.reload();
	}

	loggout(){
		this.authService.loggout();
	}
}

