import { Component, OnInit } from '@angular/core';
import { UserManagementService } from '../user-management.service';
import { ActivatedRoute, Params } from '@angular/router';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';


@Component({
	selector: 'app-user-detail',
	templateUrl: './user-detail.component.html',
	styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
	user = [];
	uid: number;
	status;
	admin: boolean;

	constructor(private authService: AuthService, private userManagementService: UserManagementService, private route: ActivatedRoute, private router: Router) {}

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.uid = +params['uid'];
		});
		this.userProfile();
	this.authService.isAdmin().subscribe(data => {this.status = data[0]['status'], this.boolAdmin(this.status)});
  }

  boolAdmin(stat){
    if(stat == "admin"){
      this.admin = true;
    }
  }
	doesExist(data){
		if(!data[0]){
			this.router.navigate(['./error']);
		}
		else {
			console.log(data);
			this.user = data;
		}
	}

	userProfile(){
		this.userManagementService.userProfile(this.uid).subscribe(
           data => {this.doesExist(data)},
           error => console.log(error),
        )
	}
}
