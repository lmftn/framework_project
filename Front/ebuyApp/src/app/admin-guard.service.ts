import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AdminGuard implements CanActivate {
	status: string;

  constructor(private authService: AuthService, private router: Router) {}

  delay(ms: number){
  	return new Promise<void>(function(resolve){
  		setTimeout(resolve, ms);
  	});
  }

  isAdmin(){
  	this.authService.isAdmin().subscribe(data => this.status = data[0]['status']);
  }

 async canActivate() {
    if(this.authService.loggedIn()) {
      this.isAdmin();
      await this.delay(1000);
      if(this.status == "admin") {
        return true;
      } else {
        this.router.navigate(['error']);
        return false;
      }
    }
    else {
      this.router.navigate(['login']);
      return false;
    }
  }
}