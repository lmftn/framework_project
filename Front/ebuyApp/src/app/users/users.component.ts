import { Component, OnInit } from '@angular/core';
import { UserManagementService } from '../user-management.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
	users = [];

  constructor(private userManagementService: UserManagementService,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
  	this.readUsers();
  }

  readUsers(){
  	this.userManagementService.usersList().subscribe(
                                                     data => this.users = data,
                                                     error => console.log(error)
                                                     )
  }

  userProfile(uid){
    this.router.navigate(['./profile/'+uid]);
  }

}
