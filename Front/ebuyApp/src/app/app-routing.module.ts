import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { RegisterComponent } from './register/register.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { LoginComponent } from './login/login.component';
import { UserUpdateComponent } from './user-update/user-update.component';
import { AuthGuard } from './auth-guard.service';
import { AdminGuard } from './admin-guard.service';
import { ErrorComponent } from './error/error.component';
import { ProfileComponent } from './profile/profile.component';
import { ModifyComponent } from './modify/modify.component';
import { ProductsComponent } from './products/products.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductUpdateComponent } from './product-update/product-update.component';
import { ProductCreateComponent } from './product-create/product-create.component';

const routes: Routes = [
{ path: 'error', component: ErrorComponent },
{ path: 'users', component: UsersComponent, canActivate: [AdminGuard]},
{ path: 'register', component: RegisterComponent },
{ path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
{ path: 'profile/:uid', component: UserDetailComponent, canActivate: [AdminGuard]},
{ path: 'login', component: LoginComponent },
{ path: 'account', component: ModifyComponent, canActivate: [AuthGuard] },
{ path: 'update/:uid', component: UserUpdateComponent, canActivate: [AdminGuard]},
{ path: 'products', component: ProductsComponent },
{ path: 'product/:pid', component: ProductDetailComponent },
{ path: 'modify/:pid', component: ProductUpdateComponent },
{ path: 'new', component: ProductCreateComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
