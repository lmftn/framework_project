var express = require('express');
var router = express();
var mysql = require('mysql')

var db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'lmftn_commerce'
})

var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({extended: false}));
router.use(bodyParser.json());

db.connect(function(err) {
  if (err) throw err
  console.log('You are now connected to your DB1 - Mysql')
});

router.post('/user', function(req, res, next){
	var now = new Date();
	var arr = [];
	req.body.creation_date = now.toISOString().slice(0,19).replace('T', ' ');
	if(!req.body.status){req.body.status = "buyer"}
	db.query('INSERT INTO user SET ?', [req.body], function(err, data){
		if(err) throw err;
		str = 'User ' + req.body.first_name + ' ' + req.body.last_name + ' created';
		arr.push(str);
		res.json(arr);
	});
});

router.get('/users', function(req, res, next){
	db.query('SELECT * FROM user', function(err, data){
		if (err) throw err;
		res.json(data);
	})
});

router.get('/user/:uid(\\d+)', function(req, res, next){
	db.query('SELECT * FROM user where id = ?', [req.params.uid], function(err, data){
		if(err) throw err;
		//data[0]['birthdate'] = data[0]['birthdate'].toISOString().substring(0, 10);
		res.json(data);
	});
});

router.post('/login', function(req, res, next){
	db.query('SELECT * FROM user WHERE email = ?', [req.body.email], function(err, data){
		if(err) throw err;
		res.json(data);
	});
});

router.put('/user/:uid(\\d+)', function(req, res, next){
	db.query('UPDATE user SET ? WHERE id = ?', [req.body, req.params.uid], function(err, data){
		if(err) throw err;
		res.json('User modified');
	});
});

router.delete('/user/:uid(\\d+)', function(req, res, next){
	db.query('DELETE FROM user WHERE id = ?', [req.params.uid], function(err, data){
		if(err) throw err;
		res.json('User successfully deleted');
	});
});

router.listen(3000, function(){ console.log('DB2 started, listening on port 3000')});
