#!/usr/bin/env python3
from appserver import app
import validators
import re
import requests
from flask import Flask, request, jsonify

def productsList():
	return requests.get('http://127.0.0.1:1800/products').json()

def productsDeleteByUser(uid):
	return requests.delete('http://127.0.0.1:1800/product/user/'+uid).json()