#!/usr/bin/env python3
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, json, jsonify
import os
#from requests import *
import requests
from flask_cors import CORS

app = Flask(__name__)

CORS(app)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
import usersController
import productsController

# ALL THE ROUTES FOR USERS #
@app.route('/register', methods=['POST'])
def userC():
    if request.method == 'POST':
        return jsonify(usersController.register(request.get_json()))


@app.route('/users', methods=['GET'])
def userRA():
    return jsonify(usersController.usersList())

@app.route('/user/<uid>', methods=['GET', 'PUT', 'DELETE'])
def userRUD(uid):
    if request.method == 'GET':
        r = usersController.userProfile(uid)
    elif request.method == 'PUT':
        r = usersController.userUpdate(request.get_json(), uid)
    elif request.method == 'DELETE':
        r = usersController.userDelete(uid)
    return jsonify(r)

# ALL THE ROUTES FOR PRODUCTS #
@app.route('/product', methods=['GET', 'POST'])
def productCR():
    if request.method == 'GET':
        r = productsController.productsList()
    elif request.method == 'POST':
        r = requests.post("http://127.0.0.1:1800/product", request.get_json()).json()
    return jsonify(r)

@app.route('/product/<pid>', methods=['GET', 'PUT', 'DELETE'])
def productRUD(pid):
    if request.method == 'GET':
        r = requests.get('http://127.0.0.1:1800/product/'+pid).json()
    elif request.method == 'PUT':
        r = requests.put('http://127.0.0.1:1800/product/'+pid, request.get_json()).json()
    elif request.method == 'DELETE':
        r = requests.delete('http://127.0.0.1:1800/product/'+pid).json()
    return jsonify(r)

@app.route('/product/user/<uid>', methods=['DELETE'])
def productDbyUID(uid):
    return jsonify(productsController.productDeleteByUser(uid))









