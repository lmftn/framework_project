#!/usr/bin/env python3
from appserver import app
import validators
import re
import requests
from flask import Flask, request, jsonify
from flask_jwt import JWT, jwt_required, current_identity
from flask_bcrypt import Bcrypt
import datetime

app.debug = True
app.config['SECRET_KEY'] = 'super-secret'
app.config['JWT_EXPIRATION_DELTA'] = datetime.timedelta(minutes=525600)
bcrypt = Bcrypt(app)


def userRegVerif(rqt):
    errorList = []
    if not validators.email(rqt['email']):
      errorList.append("Email not valid")
    if validators.length(rqt['first_name'], min=2) == False or rqt['first_name'].isalpha() == False or validators.length(rqt['last_name'], min=2) == False or rqt['last_name'].isalpha() == False:
        errorList.append("Names must contain only letters and be, at least, 2 characters")
    if rqt['password'].isalnum() == False or validators.length(rqt['password'], min=5) == False:
        errorList.append("Password must contain, at least, 5 alphanumeric (only) characters")
    if not rqt['password'] == rqt['pwdconfirmation']:
        errorList.append("Passwords must match")
    if rqt['postcode'].isnumeric() == False:
        errorList.append("Postcode must be numeric characters")
    if re.match('^[0-9]{4}[-][0-9]{2}[-][0-9]{2}$', rqt['birthdate']) == None:
        errorList.append("Invalid birthdate")
    if errorList != []:
        r = errorList
    else:
        r = False
    return r

def userUpVerif(rqt):
    errorList = []
    if not validators.email(rqt['email']):
      errorList.append("Email not valid")
    if validators.length(rqt['first_name'], min=2) == False or rqt['first_name'].isalpha() == False or validators.length(rqt['last_name'], min=2) == False or rqt['last_name'].isalpha() == False:
        errorList.append("Names must contain only letters and be, at least, 2 characters")
    if "password" in rqt:
        print('wht')
        if rqt['password'].isalnum() == False or validators.length(rqt['password'], min=5) == False:
            errorList.append("Password must contain, at least, 5 alphanumeric (only) characters")
        if not rqt['password'] == rqt['pwdconfirmation']:
            errorList.append("Passwords must match")
    if str(rqt['postcode']).isnumeric() == False:
        errorList.append("Postcode must be numeric characters")
    if errorList != []:
        r = errorList
    else:
        r = False
    return r

def register(rqt):
    req = userRegVerif(rqt)
    if req == False:
        del rqt['pwdconfirmation']
        rqt['password'] = bcrypt.generate_password_hash(rqt['password'])
        return requests.post("http://127.0.0.1:3000/user", rqt).json()
    else:
        return req

def usersList():
    return requests.get("http://127.0.0.1:3000/users").json()

def userProfile(uid):
    return requests.get('http://127.0.0.1:3000/user/'+uid).json()

def userUpdate(rqt, uid):
    req = userUpVerif(rqt)
    if req == False:
        if 'pwdconfirmation' in rqt:
            del rqt['pwdconfirmation']
        return requests.put("http://127.0.0.1:3000/user/"+uid, rqt).json()
    else:
        return req

def userDelete(uid):
    return requests.delete('http://127.0.0.1:3000/user/'+uid).json()

class User(object):
    def __init__(self, uid):
        self.id = uid

    def __str__(self):
        return "User(id='%s')" % self.id
 
def authenticate(username, password):
    data = {'email': username}
    user = requests.post('http://127.0.0.1:3000/login', data).json()
    if user:
        if bcrypt.check_password_hash(user[0]['password'], password) == True:
            return User(user[0]['id'])
 
def identity(payload):
    user_id = payload['identity']
    return {'user_id': user_id}

jwt = JWT(app, authenticate, identity)








